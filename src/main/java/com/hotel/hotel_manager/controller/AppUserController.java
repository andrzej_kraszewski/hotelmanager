package com.hotel.hotel_manager.controller;

import com.hotel.hotel_manager.service.GuestService;
import com.hotel.hotel_manager.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppUserController {

    @Autowired
    private GuestService guestService;
    @Autowired
    private ReservationService reservationService;

    @GetMapping("/")
    public String getLoginPage() {
        return "login";
    }

    @GetMapping("/calendar")
    public String getCalendar() {
        return "calendar";
    }

    @GetMapping("/guestList")
    public String getGuestList(Model model) {
        model.addAttribute("guestList", guestService.getGuestList());
        return "guestList";
    }

    @GetMapping("/reservationList")
    public String getReservationList(Model model) {
        model.addAttribute("reservationList", reservationService.findAllReservations());
        return "reservationList";
    }
}
