package com.hotel.hotel_manager.controller;
import com.hotel.hotel_manager.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RoomController {

    @Autowired
    private RoomService roomService;

    @GetMapping("/addReservation")
    public String addForm(Model model,
                          @RequestParam(required = false) String checkInDate,
                          @RequestParam(required = false) String checkOutDate,
                          @RequestParam(required = false) boolean withBreakfast,
                          @RequestParam(required = false) Double pricePerDay,
                          @RequestParam(required = false) Integer numberOfPerson,
                          @RequestParam(required = false) String guestName,
                          @RequestParam(required = false) String guestSurname,
                          @RequestParam(required = false) String documentNumber,
                          @RequestParam(required = false) String country,
                          @RequestParam(required = false) String city,
                          @RequestParam(required = false) String streetAndNumber,
                          @RequestParam(required = false) String message,
                          @RequestParam(required = false) String companyName,
                          @RequestParam(required = false) String nip) {

        model.addAttribute("checkInDate", checkInDate);
        model.addAttribute("checkOutDate", checkOutDate);
        model.addAttribute("withBreakfast", withBreakfast);
        model.addAttribute("pricePerDay", pricePerDay);
        model.addAttribute("numberOfPerson", numberOfPerson);
        model.addAttribute("guestName", guestName);
        model.addAttribute("guestSurname", guestSurname);
        model.addAttribute("documentNumber", documentNumber);
        model.addAttribute("country", country);
        model.addAttribute("city", city);
        model.addAttribute("streetAndNumber", streetAndNumber);
        model.addAttribute("companyName", companyName);
        model.addAttribute("nip", nip);

        if (message != null) {
            model.addAttribute("message", message);
        }
        return "reservationForm";
    }

    @PostMapping("/addReservation")
    public String addReservation(String checkInDate,
                                 String checkOutDate,
                                 boolean withBreakfast,
                                 Double pricePerDay,
                                 Integer numberOfPerson,
                                 String guestName,
                                 String guestSurname,
                                 String documentNumber,
                                 String country,
                                 String city,
                                 String streetAndNumber,
                                 String companyName,
                                 String nip) {

        if (roomService.addReservation(checkInDate, checkOutDate, withBreakfast, pricePerDay,
                numberOfPerson, guestName, guestSurname, documentNumber, country, city,
                streetAndNumber, companyName, nip)) {
            return "redirect:/calendar";
        }
        return "redirect:/addReservation?" +
                (checkInDate == null ? "" : "&checkInDate=" + checkInDate) +
                (checkOutDate == null ? "" : "&checkOutDate=" + checkOutDate) +
                (pricePerDay == null ? "" : "&pricePerDay=" + pricePerDay) +
                (numberOfPerson == null ? "" : "&numberOfPerson=" + numberOfPerson) +
                (guestName == null ? "" : "&guestName=" + guestName) +
                (guestSurname == null ? "" : "&guestSurname=" + guestSurname) +
                (documentNumber == null ? "" : "&documentNumber=" + documentNumber) +
                ("&message=Please fill required fields!");
    }
}