package com.hotel.hotel_manager.controller;

import com.hotel.hotel_manager.model.AppUser;
import com.hotel.hotel_manager.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin/")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

    @Autowired
    private AppUserService appUserService;

    @GetMapping("/userList")
    public String getAppUserList(Model model) {
        model.addAttribute("userList", appUserService.getAllUsers());
        return "admin/userList";
    }

    @GetMapping("/register")
    public String getRegisterPage() {
        return "admin/register";
    }

    @GetMapping
    public String getCalendarPage(){
        return "calendar";
    }

    @PostMapping("/register")
    public String register(AppUser appUser) {
        boolean success = appUserService.register(appUser.getUsername(), appUser.getPassword(), appUser.getName(), appUser.getSurname());

        if (success) {
            return "redirect:/admin/userList";
        }
        return "redirect:/admin/register";
    }

    @GetMapping("/delete/{id}")
    public String deleteUserById(@PathVariable Long id) {
        appUserService.deleteById(id);
        return "redirect:/admin/userList";
    }
}