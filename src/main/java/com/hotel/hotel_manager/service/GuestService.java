package com.hotel.hotel_manager.service;

import com.hotel.hotel_manager.model.Guest;
import com.hotel.hotel_manager.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GuestService {

    @Autowired
    private GuestRepository guestRepository;

    public Optional<Guest> findGuestByNameAndSurname(String name, String surname) {
        return guestRepository.findByNameAndSurname(name, surname);
    }

    public Optional<Guest> findGuestByCompanyName(String companyName) {
        return guestRepository.findByCompanyName(companyName);
    }

    public Optional<Guest> findByNip(String nip) {
        return guestRepository.findByNip(nip);
    }

    public Optional<Guest> findByDocumentNumber(String documentNumber) {
        return guestRepository.findByDocumentNumber(documentNumber);
    }

    public List<Guest> getGuestList() {
        return guestRepository.findAll();
    }
}