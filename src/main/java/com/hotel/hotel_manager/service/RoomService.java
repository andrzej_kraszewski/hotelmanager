package com.hotel.hotel_manager.service;

import com.hotel.hotel_manager.model.Guest;
import com.hotel.hotel_manager.model.Reservation;
import com.hotel.hotel_manager.model.Room;
import com.hotel.hotel_manager.repository.GuestRepository;
import com.hotel.hotel_manager.repository.ReservationRepository;
import com.hotel.hotel_manager.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private GuestRepository guestRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    public Optional<Room> findByRoomNumber(int roomNumber) {
        return roomRepository.findByRoomNumber(roomNumber);
    }

    public Optional<Room> findAllByNumberOfPerson(int numberOfPerson) {
        return roomRepository.findAllByNumberOfPerson(numberOfPerson);
    }

    public Optional<Room> findByRoomType(String roomType) {
        return roomRepository.findAllByRoomType(roomType);
    }

    public Optional<Room> findAllByFloor(int floor) {
        return roomRepository.findAllByFloor(floor);
    }

    public boolean addReservation(String checkInDate,
                                  String checkOutDate,
                                  boolean withBreakfast,
                                  Double pricePerDay,
                                  Integer numberOfPerson,
                                  String guestName,
                                  String guestSurname,
                                  String documentNumber,
                                  String country,
                                  String city,
                                  String streetAndNumber,
                                  String companyName,
                                  String nip) {

        if (checkInDate == null || checkInDate.isEmpty() || checkOutDate == null || pricePerDay == null || numberOfPerson == null ||
                guestName == null || guestSurname == null || documentNumber == null) {

            return false;
        } else {
            LocalDate checkIN = LocalDate.parse(checkInDate);
            LocalDate checkOut = LocalDate.parse(checkOutDate);

            Reservation reservation = new Reservation();
            Guest guest = new Guest();

            guest.setName(guestName);
            guest.setSurname(guestSurname);
            guest.setDocumentNumber(documentNumber);
            guest.setCity(city);
            guest.setStreetAndNumber(streetAndNumber);
            guest.setCountry(country);
            guest.setCompanyName(companyName);
            guest.setNip(nip);

            reservation.setCheckInDate(checkIN);
            reservation.setCheckOutDate(checkOut);
            reservation.setGuest(guest);
            reservation.setPricePerDay(pricePerDay);
            reservation.setWithBreakfast(withBreakfast);

            guestRepository.save(guest);
            reservationRepository.save(reservation);

            return true;
        }
    }
}