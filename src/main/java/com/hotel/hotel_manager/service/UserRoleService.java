package com.hotel.hotel_manager.service;

import com.hotel.hotel_manager.model.UserRole;
import com.hotel.hotel_manager.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserRole getUserRole() {
        return userRoleRepository.getByName("ROLE_USER");
    }

    public UserRole getAdminRole() {
        return userRoleRepository.getByName("ROLE_ADMIN");
    }
}