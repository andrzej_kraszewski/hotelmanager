package com.hotel.hotel_manager.service;

import com.hotel.hotel_manager.exceptions.UserNotFound;
import com.hotel.hotel_manager.model.AppUser;
import com.hotel.hotel_manager.model.UserRole;
import com.hotel.hotel_manager.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.*;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Optional<AppUser> getByName(String name) {
        return appUserRepository.findByName(name);
    }

    public Optional<AppUser> getByNameAndSurname(String name, String surname) {
        return appUserRepository.findByNameAndSurname(name, surname);
    }

    public Optional<AppUser> getByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    public boolean register(String username, String password, String name, String surname) {
        AppUser appUser = new AppUser();
        appUser.setUsername(username);
        appUser.setPassword(bCryptPasswordEncoder.encode(password));
        appUser.setName(name);
        appUser.setSurname(surname);
        appUser.getUserRoles().add(userRoleService.getUserRole());

        try {
            appUserRepository.saveAndFlush(appUser);
        } catch (ConstraintViolationException cve) {
            return false;
        }
        return true;
    }

    public void deleteById(Long id) {
        appUserRepository.deleteById(id);
    }

    public List<UserRole> getRolesByUsername(String username) {
        Optional<AppUser> userOptional = getByUsername(username);
        if (userOptional.isPresent()) {
            return new LinkedList<>(userOptional.get().getUserRoles());
        }
        throw new UserNotFound();
    }
}