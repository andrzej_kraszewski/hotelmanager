package com.hotel.hotel_manager.service;

import com.hotel.hotel_manager.model.Reservation;
import com.hotel.hotel_manager.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;

@Service
public class ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;


    public Optional<Reservation> getByStartDate(LocalDate startDate) {
        return reservationRepository.findByCheckInDate(startDate);
    }

    public Optional<Reservation> getByEndDate(LocalDate endDate) {
        return reservationRepository.findByCheckOutDate(endDate);
    }

    public double getFullPrice(double pricePerNight, double priceForBreakfast, int numberOfPerson, LocalDate dateStart, LocalDate dateEnd) {
        Period period = Period.between(dateEnd, dateStart);
        int days = period.getDays();

        double fullPrice = days * pricePerNight + ((numberOfPerson * priceForBreakfast) * days);

        return fullPrice;
    }

    public List<Reservation> findAllReservations() {
        List<Reservation> reservations = reservationRepository.findAll();

        Collections.sort(reservations, new Comparator<Reservation>() {
            @Override
            public int compare(Reservation o1, Reservation o2) {
                return o1.getCheckInDate().compareTo(o2.getCheckInDate());
            }
        });

        return reservations;
    }
}