package com.hotel.hotel_manager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = true)
    private LocalDate checkInDate;
    @Column(nullable = true)
    private LocalDate checkOutDate;
    @Column(nullable = true)
    private boolean withBreakfast;
    @Column(nullable = true)
    private Double pricePerDay;
    @Column(nullable = true)
    private Double totalPrice;

    @ManyToOne
    private Guest guest;
}
