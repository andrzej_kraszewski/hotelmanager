package com.hotel.hotel_manager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Guest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = true)
    private String name;
    @Column(nullable = true)
    private String surname;
    @Column(nullable = true)
    private String documentNumber;

    private String country;
    private String city;
    private String streetAndNumber;
    private String companyName;
    private String nip;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Reservation> reservationList;
}