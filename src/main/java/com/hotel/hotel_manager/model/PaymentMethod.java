package com.hotel.hotel_manager.model;

public enum PaymentMethod {
    CREDIT_CARD, CASH, TRANSFER
}
