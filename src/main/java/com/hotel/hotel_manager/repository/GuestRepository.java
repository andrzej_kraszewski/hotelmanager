package com.hotel.hotel_manager.repository;

import com.hotel.hotel_manager.model.Guest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {

    Optional<Guest> findByNameAndSurname(String name, String surname);

    Optional<Guest> findByCompanyName(String companyName);

    Optional<Guest> findByNip(String nip);

    Optional<Guest> findByDocumentNumber(String documentNumber);
}
