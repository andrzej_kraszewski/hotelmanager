package com.hotel.hotel_manager.repository;

import com.hotel.hotel_manager.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    Optional<Reservation> findByCheckInDate(LocalDate checkInDate);

    Optional<Reservation> findByCheckOutDate(LocalDate checkOutDate);
}
