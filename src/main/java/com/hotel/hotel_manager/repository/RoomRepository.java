package com.hotel.hotel_manager.repository;

import com.hotel.hotel_manager.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    Optional<Room> findByRoomNumber(int roomNumber);

    Optional<Room> findAllByNumberOfPerson(int numberOfPerson);

    Optional<Room> findAllByRoomType(String roomType);

    Optional<Room> findAllByFloor(int floor);
}
