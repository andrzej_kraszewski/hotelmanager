package com.hotel.hotel_manager.repository;

import com.hotel.hotel_manager.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByName(String name);

    Optional<AppUser> findByUsername(String username);

    Optional<AppUser> findByNameAndSurname(String name, String surname);

    Optional<AppUser> deleteByNameAndSurname(String name, String surname);
}
