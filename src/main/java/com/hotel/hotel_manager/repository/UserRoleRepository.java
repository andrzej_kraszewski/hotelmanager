package com.hotel.hotel_manager.repository;

import com.hotel.hotel_manager.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    UserRole getByName(String role_user);

    Optional<UserRole> findByName(String role);
}
